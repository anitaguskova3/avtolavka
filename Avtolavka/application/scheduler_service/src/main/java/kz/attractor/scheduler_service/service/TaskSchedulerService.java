package kz.attractor.scheduler_service.service;

import kz.attractor.datamodel.model.Task;
import kz.attractor.datamodel.model.TaskStatus;
import kz.attractor.datamodel.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskSchedulerService {

    private final TaskRepository taskRepository;

    @Scheduled(cron = "0 0 */10 * * *")
    public void remove(){
            TaskStatus status = TaskStatus.TASK_CLOSED;
            log.info("FindByStatus : Success");
            List<Task> tasks = taskRepository.findByStatus(status);
            log.info("Remove task : Success");
            tasks.removeIf(task -> (task.getFinish().isAfter(task.getFinish().plusDays(30))));
    }
}
