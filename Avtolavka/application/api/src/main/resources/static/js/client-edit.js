'use strict';
showPhone2();
showAddress();
showEmail2();
showEmail3();
showPhone3();
selectMainEmail();
selectMainPhone();

function showPhone2() {
    $('.phone1').find('.btnPhoneAdd').click(function () {
        $('.phone2').removeAttr('hidden');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $(this).empty();
    })
}

function showAddress() {
    $('.legal_address').find('.addressBtn').click(function () {
        $('.addressBtnForm').removeAttr('hidden');
        $(this).remove();
    })
}

function showEmail2() {
    $('.email1').find('.btnEmailAdd').click(function () {
        $('.email2').removeAttr('hidden');
        $(this).remove();
    })
}
function showEmail3() {
    $('.email2').find('.btnEmailAdd').click(function () {
        $('.email3').removeAttr('hidden');
        $(this).remove();
    })
}

function showPhone3() {
    $('.phone2').find('.btnPhoneAdd').click(function () {
        $('.phone3').removeAttr('hidden');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $(this).empty();
    })
}

function selectMainPhone() {
    $('.phoneStatus').click(function () {
        $('.phoneMain').val($(this).parent().find('input').val());
    })
}

function selectMainEmail() {
    $('.emailStatus').click(function () {
        $('.emailMain').val($(this).parent().find('input').val());
    })
}