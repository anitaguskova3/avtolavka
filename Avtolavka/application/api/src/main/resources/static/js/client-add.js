'use strict';
const server = window.location.origin;
const serverApi = server + "/api/";

requestBankNames();

function setHandlers() {
    showPhone2();
    showPhone3();
    selectMainPhone();
    showEmail2();
    showEmail3();
    selectMainEmail();
    sendAddClientRequest();
}

function main(banks, clientsKbs) {
    showPageLayout(banks, clientsKbs);
    setHandlers()
}

function sendAddClientRequest() {
    $('#addClientForm').submit(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#phoneMain').val($('.phoneMain').val())
        $('#emailMain').val($('.emailMain').val())
        if($('#clientKbeSelector').val() == "") {
            alert('Выберите префикс');
            return;
        }
        if ($('#name').val() == "") {
            alert('Заполните наименование');
            return;
        }
        if ($('#shortName').val() == "") {
            alert('Заполните краткое наименование');
            return;
        }
        if ($('#banksSelector').val() == "") {
            alert('Выберите банк');
            return;
        }

        $.ajax({
            method: "POST",
            url: serverApi + "clients/add",
            data: $('#addClientForm').serialize(),
            success: (response) => {
                console.log('success');
                window.location = window.location.origin + "/clients";
            },
            error: (error) => {
                alert(error)
            }
        })
    })
}

const btns = $(".addressBtn")
const btnsForm = $(".addressBtnForm")

for (let i = 0; i < btns.length; i++) {
    btns.eq(i).on("click", () => {
        btnsForm.eq(i).toggleClass("addressHide")
    })
}

function showPhone2() {
    $('.phone1').find('.btnPhoneAdd').click(function () {
        $('.phone2').removeAttr('hidden');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $(this).empty();
    })
}

function showPhone3() {
    $('.phone2').find('.btnPhoneAdd').click(function () {
        $('.phone3').removeAttr('hidden');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $(this).empty();
    })
}

function selectMainPhone() {
    $('.phoneStatus').click(function () {
        $('.phoneStatus').find('span').text('Сделать основным');
        $('.phoneStatus').addClass('btn-xs');
        $('.phoneStatus').addClass('rounded-1');
        $('.phoneStatus').addClass('btn-primary');
        $(this).find('span').text('Основной');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $('.phoneStatus').parent().find('input').removeClass('phoneMain');
        $(this).parent().find('input').addClass('phoneMain');
    })
}

function showEmail2() {
    $('.email1').find('.btnEmailAdd').click(function () {
        $('.email2').removeAttr('hidden');
        $(this).remove();
    })
}

function showEmail3() {
    $('.email2').find('.btnEmailAdd').click(function () {
        $('.email3').removeAttr('hidden');
        $(this).remove();
    })
}

function selectMainEmail() {
    $('.emailStatus').click(function () {
        $('.emailStatus').find('span').text('Сделать основным');
        $('.emailStatus').addClass('btn-xs');
        $('.emailStatus').addClass('rounded-1');
        $('.emailStatus').addClass('btn-primary');
        $(this).find('span').text('Основной');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $('.emailStatus').parent().find('input').removeClass('emailMain');
        $(this).parent().find('input').addClass('emailMain');
    })
}

function requestClientKbeNames(banks) {
    $.ajax({
        method: "GET",
        url: serverApi + "clientKbes",
        success: (response) => {
            console.log(response)
            main(banks, response)
        },
        error: (error) => {
            console.log(error);
        }
    })
}

function requestBankNames() {
    $.ajax({
        method: "GET",
        url: serverApi + "banks",
        success: (banks) => {
            requestClientKbeNames(banks)
        },
        error: (error) => {
            console.log(error);
        }
    })
}

function showPageLayout(banks, clientKbes) {
    addBanks(banks);
    addClientKbe(clientKbes);
}

function addBanks(banks) {
    for (let i = 0; i < banks.length; i++) {
        $('#banksSelector').append($(`
            <option value="${banks[i]}">${banks[i]}</option>
        `));
    }
}

function addClientKbe(clientKbes) {
    for (let i = 0; i < clientKbes.length; i++) {
        $('#clientKbeSelector').append($(`
            <option value="${clientKbes[i].id}">${clientKbes[i].shortName}</option>
        `));
    }
}