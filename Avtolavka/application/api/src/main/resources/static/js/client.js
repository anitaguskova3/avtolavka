'use strict';
const server = window.location.origin;
const serverApi = server + "/api/";
let clientId = $('#containerClientEdit').attr('data-client-id');

setHandlers();

function setHandlers() {
    showAddress();
    showPhone1();
    showPhone2();
    showPhone3();
    showEmail1();
    showEmail2();
    showEmail3();
    showRequisites();
}

function showAddress() {
    $('.address').find('.btnShowAddress').on("click", function () {
        $('.physicalAddress').toggleClass('addressHide');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
    })
}

function showPhone1() {
    $('.phone').find('.btnPhoneAdd').on("click", function () {
        $('.phone1').toggleClass('phoneHide');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
    })
}

function showPhone2() {
    $('.phone1').find('.btnPhoneAdd').on("click", function () {
        $('.phone2').toggleClass('phoneHide');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
    })
}

function showPhone3() {
    $('.phone2').find('.btnPhoneAdd').on("click", function () {
        $('.phone3').toggleClass('phoneHide');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
    })
}

function showEmail1() {
    $('.email').find('.btnEmailAdd').on("click", function () {
        $('.email1').toggleClass('emailHide');
    })
}

function showEmail2() {
    $('.email1').find('.btnEmailAdd').on("click", function () {
        $('.email2').toggleClass('emailHide');
    })
}

function showEmail3() {
    $('.email2').find('.btnEmailAdd').on("click", function () {
        $('.email3').toggleClass('emailHide');
    })
}

function showRequisites() {
    $('.kbe').find('.btnRequisites').on("click", function () {
        $('.requisites').toggleClass('requisitesHide');
    })
}