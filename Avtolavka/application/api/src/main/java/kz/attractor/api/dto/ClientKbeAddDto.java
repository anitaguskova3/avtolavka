package kz.attractor.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientKbeAddDto {

    private String name;
    private String shortName;
    private int kbe;
}
