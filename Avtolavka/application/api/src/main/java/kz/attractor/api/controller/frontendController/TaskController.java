package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.TaskAddSubtaskDto;
import kz.attractor.api.dto.TaskCommentDto;
import kz.attractor.api.dto.TaskDto;
import kz.attractor.api.dto.TaskDtoAdd;
import kz.attractor.api.service.PropertiesService;
import kz.attractor.api.service.TaskCommentService;
import kz.attractor.api.service.TaskService;
import kz.attractor.datamodel.model.TaskPriority;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final TaskService service;
    private final PropertiesService propertiesService;
    private final TaskCommentService taskCommentService;


    @GetMapping("/")
    public String showTasksPage(Model model,
                                @PageableDefault(sort = {"status"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable, HttpServletRequest uriBuilder) {
        Page<TaskDto> tasks = service.findAll(pageable);
        var uri  = uriBuilder.getRequestURI();
        constructPageable(tasks, propertiesService.getDefaultPageSize(), model, uri);
        model.addAttribute("page", tasks);
        return "tasks";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink",
                    constructPageUri(uri,
                            list.nextPageable().getPageNumber(),
                            list.nextPageable().getPageSize())
            );
        }
        if (list.hasPrevious()) {
            model.addAttribute("nextPreviousLink",
                    constructPageUri(uri,
                            list.previousPageable().getPageNumber(),
                            list.previousPageable().getPageSize())
            );
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrevious", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        if (uri.contains("?")) {
            return String.format("%s&page=%s&size=%s", uri, page, size);
        }
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping("/tasks/{id}")
    public String showTask(@PathVariable long id, Model model) {
        TaskDto task = service.findById(id);
        model.addAttribute("task", task);
        TaskDto parentTask = service.findParentTaskOrNull(task.getId());
        model.addAttribute("parentTask", parentTask);
        List<TaskCommentDto> comments = taskCommentService.findAllByTask_Id(id);
        model.addAttribute("comments", comments);
        return "task";
    }

    @GetMapping("/tasks/{id}/edit")
    public String edit(@PathVariable long id, Model model) {
        TaskDto task = service.findById(id);
        model.addAttribute("form", task);
        model.addAttribute("statuses", TaskPriority.values());
        return "task-edit";
    }

    @PostMapping("task-edit")
    public String edit(@Valid TaskDto form,
                       BindingResult validationResult,
                       RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/" + form.getId() + "/edit";
        }
        service.update(form);
        return "redirect:/";
    }

    @GetMapping("/tasks/add")
    public String add() {
        return "task-add";
    }

    @PostMapping("task-add")
    public String add(@Valid TaskDtoAdd form,
                      BindingResult validationResult,
                      RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/add";
        }
        service.add(form);
        return "redirect:/";
    }

    @PostMapping("/task/close/{id}")
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String closeOrderStatus(@PathVariable long id) {
        service.closeStatus(id);
        return "redirect:/";
    }

    @GetMapping("/tasks/{id}/add-subtask")
    public String addSubtask(@PathVariable long id, Model model) {
        TaskDto parentTask = service.findById(id);
        model.addAttribute("parentTask", parentTask);
        return "task-add-subtask";
    }

    @PostMapping("task-add-subtask")
    public String addSubtask(@Valid TaskAddSubtaskDto form,
                             BindingResult validationResult,
                             RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/add";
        }
        service.addSubtask(form);
        return "redirect:/";
    }
}
