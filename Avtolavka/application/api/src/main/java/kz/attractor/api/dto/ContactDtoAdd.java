package kz.attractor.api.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactDtoAdd {
    private String name;
    private String phoneMain;
    private String phone1;
    private String phone2;
    private String phone3;
    private String emailMain;
    private String email1;
    private String email2;
    private String email3;
    private String position;
    private long clientId;
    private String date;
    private String dateOfBirth;
}
