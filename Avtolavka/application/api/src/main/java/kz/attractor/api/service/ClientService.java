package kz.attractor.api.service;

import kz.attractor.api.dto.ClientAddressAddDto;
import kz.attractor.api.dto.ClientAddressDto;
import kz.attractor.api.dto.ClientDto;
import kz.attractor.api.dto.ClientDtoAdd;
import kz.attractor.api.price.creator.PriceExcelFileCreator;
import kz.attractor.api.price.sender.MailPriceSender;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.ClientRepository;
import kz.attractor.datamodel.util.SearchCriteria;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Slf4j
@Service
@Getter
@Setter
@RequiredArgsConstructor
public class ClientService {
    private final ClientRepository clientRepository;
    private final ClientKbeService clientKbeService;
    private final ClientAddressService clientAddressService;
    private final EmailService emailService;
    private final ProductService productService;
    private final PriceExcelFileCreator priceExcelFileCreator;
    private final MailPriceSender priceSender;
    private AtomicLong counter = new AtomicLong();

    public Page<ClientDto> findAll(Pageable pageable) {
        Page<Client> clients = clientRepository.findAll(pageable);
        return new PageImpl<ClientDto>(
                clients.getContent().stream()
                        .map(ClientDto::from)
                        .sorted(Comparator.comparing(ClientDto::getId).reversed())
                        .collect(Collectors.toList()),
                pageable, clients.getTotalElements()
        );
    }

    public List<ClientDto> findAll() {
        var clients = clientRepository.findAll()
                .stream()
                .map(ClientDto::from)
                .collect(Collectors.toList());
        clients.sort(Comparator.comparing(ClientDto::getId));
        return clients;
    }

    public List<ClientDto> findAllSearched(String query) {
        ClientSpecification nameLike = new ClientSpecification(new SearchCriteria("name", ":", query));
        ClientSpecification shortName = new ClientSpecification(new SearchCriteria("shortName", ":", query));
        var clients = clientRepository.findAll(Specification.where(nameLike).or(shortName))
                .stream()
                .map(ClientDto::from)
                .collect(Collectors.toList());
        clients.sort(Comparator.comparing(ClientDto::getStatus).reversed());
        return clients;
    }

    public ClientDto findById(long id) {
        var client = clientRepository.findById(id).orElseThrow(() -> new EntityMissingException(Client.class, id));
        return ClientDto.from(client);
    }

    public ClientDto update(ClientDto form, ClientAddressDto clientAddressDto) {
        var clientOpt = clientRepository.findById(form.getId());
        if (clientOpt.isEmpty()) {
            return null;
        }
        ClientAddress clientAddress = clientAddressService.update(clientAddressDto);

        Client client = clientOpt.get();
        client.setName(form.getName());
        client.setBin(form.getBin());
        client.setBank(form.getBank());
        client.setClientKbe(form.getClientKbe());
        client.setShortName(form.getShortName());
        client.setAccountNumber(form.getAccountNumber());
        client.setAddress(clientAddress);
        client.setPhoneMain(form.getPhoneMain());
        client.setPhone1(form.getPhone1());
        client.setPhone2(form.getPhone2());
        client.setPhone3(form.getPhone3());
        client.setEmailMain(form.getEmailMain());
        client.setEmail1(form.getEmail1());
        client.setEmail2(form.getEmail2());
        client.setEmail3(form.getEmail3());
        client.setStatus(ClientStatus.valueOfLabel(form.getStatus()));

        return ClientDto.from(clientRepository.save(client));
    }

    public ClientDto add(ClientDtoAdd form, ClientAddressAddDto address) {
        ClientAddress clientAddress = clientAddressService.add(address);
        Client client = Client.builder()
                .bin(form.getBin())
                .name(form.getClientKbe().getName()+ " " +form.getName())
                .resident(form.getResident())
                .shortName(form.getClientKbe().getShortName()+ " " +form.getShortName())
                .accountNumber(form.getAccountNumber())
                .clientKbe(form.getClientKbe())
                .address(clientAddress)
                .bin(form.getBin())
                .phoneMain(form.getPhoneMain())
                .phone1(form.getPhone1())
                .phone2(form.getPhone2())
                .phone3(form.getPhone3())
                .emailMain(form.getEmailMain())
                .email1(form.getEmail1())
                .email2(form.getEmail2())
                .email3(form.getEmail3())
                .status(ClientStatus.CLIENT_NEW)
                .bank(ClientBank.valueOfBankName(form.getBank()))
                .createDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .build();
        Client savedClient = clientRepository.save(client);
        return ClientDto.from(savedClient);
    }

    public void delete(Client client){

        clientRepository.delete(client);
    }

}