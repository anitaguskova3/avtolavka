package kz.attractor.api.config;

import kz.attractor.common.exception.EntityMissingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = EntityMissingException.class)
    public ResponseEntity<Object> handleException(EntityMissingException exception) {
        return handleThrowable(exception, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = Throwable.class)
    public ResponseEntity<Object> handleException(Throwable exception) {
        return handleThrowable(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    protected ResponseEntity<Object> handleThrowable(Throwable t, HttpStatus status) {
        log.error("Error occurred", t);
        return new ResponseEntity<>(new ErrorResponse(t), status);
    }
}