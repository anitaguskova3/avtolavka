package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Document;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDto {

    private long id;
    private String title;
    private String file;
    private String date;

    public static DocumentDto from(Document document){
        return DocumentDto.builder()
                .id(document.getId())
                .title(document.getTitle())
                .date(document.getTitle())
                .file(document.getFile())
                .build();
    }
}
