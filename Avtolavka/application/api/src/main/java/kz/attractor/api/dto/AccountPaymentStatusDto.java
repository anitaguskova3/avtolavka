package kz.attractor.api.dto;

import kz.attractor.datamodel.model.AccountPaymentStatus;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountPaymentStatusDto {
    private Long id;

    @NotBlank(message = "Поле не должно быть пустым")
    private String name;

    private double sumPayment;

    public static AccountPaymentStatus from(AccountPaymentStatus accountPaymentStatus){
        return AccountPaymentStatus.builder()
                .id(accountPaymentStatus.getId())
                .name(accountPaymentStatus.getName())
                .sumPayment(accountPaymentStatus.getSumPayment())
                .build();
    }
}
