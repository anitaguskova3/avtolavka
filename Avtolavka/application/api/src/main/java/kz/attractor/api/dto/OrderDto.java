package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Company;
import kz.attractor.datamodel.model.Order;
import kz.attractor.datamodel.model.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.temporal.ChronoUnit;

@Getter
@Setter
@Builder
public class OrderDto {
    private long id;
    private String dateCreation;
    private String client;
    private boolean isClosed;
    private Company company;
    private User user;

    public static OrderDto from(Order order) {
        return OrderDto.builder()
                .id(order.getId())
                .dateCreation(order.getDateCreation().truncatedTo(ChronoUnit.SECONDS).toString())
                .client(order.getClient().getName())
                .isClosed(order.isClosed())
                .company(order.getCompany())
                .user(order.getUser())
                .build();
    }
}
