package kz.attractor.api.price.sender;

import kz.attractor.datamodel.model.Client;

import java.io.File;

public interface PriceSender<T> {
    void send(File file, Client client, T sender);
}
