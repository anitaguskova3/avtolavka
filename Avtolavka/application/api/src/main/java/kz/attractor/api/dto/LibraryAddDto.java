package kz.attractor.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LibraryAddDto {

    @NotBlank(message = "Поле не должно быть пустым")
    private String title;
    private String subtitle;


    @NotBlank(message = "Поле не должно быть пустым")
    private String descriptionPromo;
    @NotBlank(message = "Поле не должно быть пустым")
    private String imagePromo;

    private String image;

    private String description;
    private String file;
}
