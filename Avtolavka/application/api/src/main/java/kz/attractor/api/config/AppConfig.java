package kz.attractor.api.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    @ConfigurationProperties(prefix = "storage")
    public StorageProperties getStorageProperties() {
        return new StorageProperties();
    }

}
