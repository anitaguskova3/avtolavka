package kz.attractor.api.dto;

import kz.attractor.datamodel.model.ClientBank;
import kz.attractor.datamodel.model.Company;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDto {
    private Long id;
    private String bin;
    private String name;
    private int kbe;
    private int cato;
    private int ocpo;
    private String nds;
    private String bank;
    private String bik;
    private String iik;
    private String legal_address;
    private String physical_address;
    private String index;
    private String emailBuh;
    private String phone;
    private String krp;
    private String oked;
    private String emailPrice;
    private String fioDirector;
    private String phoneDirector;

    public static CompanyDto from(Company company){
        return CompanyDto.builder()
                .id(company.getId())
                .bik(company.getBik())
                .bin(company.getBin())
                .name(company.getName())
                .kbe(company.getKbe())
                .cato(company.getCato())
                .ocpo(company.getOcpo())
                .nds(company.getNds())
                .bank(company.getBank())
                .iik(company.getIik())
                .legal_address(company.getLegal_address())
                .physical_address(company.getPhysical_address())
                .index(company.getIndex())
                .emailBuh(company.getEmailBuh())
                .phone(company.getPhone())
                .krp(company.getKrp())
                .oked(company.getOked())
                .emailPrice(company.getEmailPrice())
                .fioDirector(company.getFioDirector())
                .phoneDirector(company.getPhoneDirector())
                .build();
    }
}
