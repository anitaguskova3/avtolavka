package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.ClientRequestDto;
import kz.attractor.api.dto.OrderDtoAdd;
import kz.attractor.api.service.ClientRequestService;
import kz.attractor.api.service.OrderExcelservice.OrderExcelService;
import kz.attractor.api.service.OrderService;
import kz.attractor.api.service.StorageService;
import kz.attractor.datamodel.model.Account;
import kz.attractor.datamodel.model.Order;
import kz.attractor.datamodel.model.OrderProduct;
import kz.attractor.datamodel.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/orders")
public class OrderApiController {
    private final OrderService orderService;
    private final ClientRequestService requestService;
    private final AccountRepository accountRepository;
    private final OrderExcelService orderExcelService;

    @GetMapping("/{id}")
    public HashMap<String, Object> getOrderById(@PathVariable Long id) {
        Order order = orderService.findById(id);
        List<OrderProduct> orderProducts = orderService.findOrderProductsByOrderId(id);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("order", order);
        hashMap.put("orderCreateDate", order.getDateCreation().truncatedTo(ChronoUnit.SECONDS));
        hashMap.put("orderProducts", orderProducts);
        return hashMap;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addOrder(@RequestBody OrderDtoAdd orderDtoAdd) {
        orderService.add(orderDtoAdd);
    }

    @GetMapping("/add")
    List<ClientRequestDto> showAddOrdersPage() {
        return requestService.findAll();
    }

    @PostMapping("/add/save-request")
    void saveOriginalRequest(@RequestBody ClientRequestDto dto){
        requestService.save(dto);
    }

    @PostMapping("/edit")
    @ResponseStatus(HttpStatus.CREATED)
    public void editOrder(@RequestBody OrderDtoAdd orderDtoAdd) {
        orderService.edit(orderDtoAdd);
    }

    @PostMapping("/bonus")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void handleBonus(@RequestParam long orderId, @RequestParam boolean bonus) {
        orderService.handleBonus(orderId, bonus);
    }
//    @SneakyThrows
//    @GetMapping("/download/{id}")
//    public void outExcel (HttpServletResponse response, @PathVariable long id) throws Exception {
//        String exportName = "Order" ;
//
//        response.setContentType("application/vnd.ms-excel");
//        response.addHeader("Content-Disposition", "attachment;filename="+
//                URLEncoder.encode(exportName, "UTF-8") + ".xlsx");
//        String[] headList = new String[]{"Название", "Цена", "Количество", "Ед.измерения", "Сумма"} ;
//        orderExcelService.exportExcel(headList,id,response.getOutputStream()) ;
////        InputStreamResource inputStream = new InputStreamResource(new FileInputStream(exportName));
////        FileCopyUtils.copy(inputStream, response.getOutputStream());
//
//    }

//    @GetMapping(value = "/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
//    public ResponseEntity<StreamingResponseBody> downloadLog() throws FileNotFoundException {
//        InputStream inputStream = new FileInputStream("/path/to/log");
//        StreamingResponseBody body = outputStream -> FileCopyUtils.copy(inputStream, outputStream);
//        return ResponseEntity.ok()
//                .header("Content-Disposition", "attachment;filename=server.log")
//                .body(body);

}
