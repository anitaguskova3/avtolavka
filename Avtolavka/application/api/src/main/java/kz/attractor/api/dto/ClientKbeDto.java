package kz.attractor.api.dto;

import kz.attractor.datamodel.model.ClientKbe;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientKbeDto {

    private long id;

    @NotBlank(message = "Поле не должно быть пустым")
    private String name;

    @NotBlank(message = "Поле не должно быть пустым")
    private String shortName;

    @NotBlank(message = "Поле не должно быть пустым")
    private int kbe;

    public static ClientKbeDto from (ClientKbe clientKbe){
        return ClientKbeDto.builder()
                .id(clientKbe.getId())
                .name(clientKbe.getName())
                .shortName(clientKbe.getShortName())
                .kbe(clientKbe.getKbe())
                .build();
    }
}
