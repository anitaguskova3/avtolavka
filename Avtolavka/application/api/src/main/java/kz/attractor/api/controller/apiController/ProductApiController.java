package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.ProductDto;
import kz.attractor.api.service.ProductService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/products")
public class ProductApiController {
    private final ProductService service;

    @GetMapping("{id}")
    public ProductDto getProductById(@PathVariable String id) {
        var productId = Integer.parseInt(id);
        return service.findById(productId);
    }


    @GetMapping
    public Page<ProductDto> findProduct(@PageableDefault(sort = {"name"}, direction = Sort.Direction.DESC, size = 20)
                                               Pageable pageable, String query) {
        return service.findAll(query, pageable);
    }

    @GetMapping("/search")
    public List<ProductDto> showProductsPage(@RequestParam(required = false) String query) {
        if (query != null) {
            return service.findAll(query);
        }
        return service.findAll();
    }

    @PostMapping("/tag")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void addTags(@RequestParam int productId, @RequestParam String tag) {
        service.addTag(productId, tag);
    }
}