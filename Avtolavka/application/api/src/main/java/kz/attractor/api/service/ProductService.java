package kz.attractor.api.service;

import kz.attractor.api.config.Param;
import kz.attractor.api.dto.*;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.ProductRepository;
import kz.attractor.datamodel.util.SearchCriteria;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductNameService productNameService;

    public Page<ProductDto> findAll(String query, Pageable pageable) {
        if (query != null) {
            ProductSpecification nameLike = new ProductSpecification(new SearchCriteria("article", ":", query));
            ProductSpecification mah = new ProductSpecification(new SearchCriteria("name", ":", query));
            ProductSpecification tag = new ProductSpecification(new SearchCriteria("tag", ":", query));
            ProductSpecification articleSearch = new ProductSpecification(new SearchCriteria("articleSearch", ":", query));
            Page<Product> productPage = productRepository.findAll(Specification.where(nameLike).or(mah).or(tag).or(articleSearch), pageable);
            return new PageImpl<ProductDto>(
                    productPage.getContent().stream()
                            .map(ProductDto::from)
                            .sorted(Comparator.comparing(ProductDto::getPrice))
                            .collect(Collectors.toList()),
                    pageable, productPage.getTotalElements()
            );
        }
        Page<Product> productPage = productRepository.findAll(pageable);
        return new PageImpl<ProductDto>(
                productPage.getContent().stream()
                        .map(ProductDto::from)
                        .collect(Collectors.toList()),
                pageable, productPage.getTotalElements()
        );
    }

    public List<ProductDto> findAll(String query) {
        if(query != null){
            ProductSpecification nameLike = new ProductSpecification(new SearchCriteria("article", ":", query));
            ProductSpecification mah = new ProductSpecification(new SearchCriteria("name", ":", query));
            ProductSpecification tag = new ProductSpecification(new SearchCriteria("tag", ":", query));
            ProductSpecification articleSearch = new ProductSpecification(new SearchCriteria("articleSearch", ":", query));
            List<Product> productPage = productRepository.findAll(Specification.where(nameLike).or(mah).or(tag).or(articleSearch));
            return productPage.stream()
                    .map(ProductDto::from)
                    .sorted(Comparator.comparing(ProductDto::getPrice))
                    .collect(Collectors.toList());
        }
        return null;
    }

    public List<ProductDto> findAll() {
        return productRepository.findAll().stream()
                .map(ProductDto::from)
                .collect(Collectors.toList());
    }

    public ProductDto findById(int id) {
        var product = productRepository.findById(id).orElseThrow( () ->
                new EntityMissingException(Product.class, id));
        return ProductDto.from(product);
    }

    public List<ProductDto> findAllForPriceSend() {
        List<Product> products = productRepository.findAllByWarehouse_IncludeToPriceList(true);
        return products.stream().map(ProductDto::from)
                .collect(Collectors.toList());
    }

    public void add(ProductDtoAdd productDto, ProductNameAddDto productNameAddDto){
        ProductName productName = productNameService.add(productNameAddDto);
        Product product = Product.builder()
                .name(productDto.getName())
                .article(productDto.getArticle())
                .articleSearch(productDto.getArticleSearch())
                .quantity(productDto.getQuantity())
                .image(productDto.getImage())
                .dateEdit(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .unit(productDto.getUnit())
                .purchasePrice(productDto.getPurchasePrice())
                .sellingPrice(sellingPrice(productDto.getPurchasePrice()))
                .status(ProductStatus.IN_STOCK)
                .quality(ProductQuality.NEW)
                .tag(productDto.getTag())
                .warehouse(productDto.getWarehouse())
                .productName(productName)
                .manufacturer(productDto.getManufacturer())
                .description(productDto.getDescription())
                .build();
        productRepository.save(product);

    }

    public ProductDto update(ProductDto form, ProductNameDto formName) {
        var productOpt = productRepository.findById(form.getId());
        if (productOpt.isEmpty()) {
            return null;
        }
        ProductName productName = productNameService.update(formName);

        Product product = productOpt.get();
        product.setName(form.getName());
        product.setArticle(form.getArticle());
        product.setArticleSearch(form.getArticleSearch());
        product.setQuantity(form.getQuantity());
        product.setDateEdit(form.getDateEdit());
        product.setTag(form.getTag());
        product.setQuality(ProductQuality.valueOfLabel(form.getQuality()));
        product.setStatus(ProductStatus.valueOfLabel(form.getStatus()));
        product.setProductName(productName);
        product.setWarehouse(form.getWarehouse());
        product.setImage(form.getImage());
        product.setUnit(form.getUnit());
        product.setPurchasePrice(form.getPurchasePrice());
        product.setSellingPrice(sellingPrice(form.getPurchasePrice()));
        product.setDescription(form.getDescription());
        product.setManufacturer(form.getManufacturer());

        return ProductDto.from(productRepository.save(product));
    }

    public void addTag(int productId, String tag) {
        Product product = productRepository.findById(productId).get();
        product.setTag(product.getTag() + " " + tag);
        productRepository.save(product);
    }

    public void addQuality(int id, String quality){
        Product product = productRepository.findById(id).get();
        product.setQuality(ProductQuality.valueOfLabel(quality));
        productRepository.save(product);
    }

    public BigDecimal sellingPrice(BigDecimal purchasePrice){
        BigDecimal sellingPrice = purchasePrice
                .multiply(BigDecimal.valueOf(1 + Param.RATE_FIRST)
                        .multiply(BigDecimal.valueOf(1 + Param.RATE_SECOND)));
        return sellingPrice;
    }
}