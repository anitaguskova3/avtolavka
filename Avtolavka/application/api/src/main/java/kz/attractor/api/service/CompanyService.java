package kz.attractor.api.service;

import kz.attractor.api.dto.CompanyDto;
import kz.attractor.datamodel.model.Company;
import kz.attractor.datamodel.repository.CompanyRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Getter
@Setter
@RequiredArgsConstructor
public class CompanyService {

    private final CompanyRepository companyRepository;

    public List<CompanyDto> findAll() {
        List<Company> companies = companyRepository.findAll();
        return companies.stream()
                .map(CompanyDto::from)
                .collect(Collectors.toList());
    }

    public CompanyDto findById(long id) {
        var company = companyRepository.findById(id).orElse(null);
        return CompanyDto.from(company);
    }

    public void add(CompanyDto form){
        Company company = Company.builder()
                .bin(form.getBin())
                .name(form.getName())
                .bank(form.getBank())
                .emailPrice(form.getEmailPrice())
                .emailBuh(form.getEmailBuh())
                .bik(form.getBik())
                .cato(form.getCato())
                .fioDirector(form.getFioDirector())
                .kbe(form.getKbe())
                .krp(form.getKrp())
                .index(form.getIndex())
                .iik(form.getIik())
                .legal_address(form.getLegal_address())
                .physical_address(form.getPhysical_address())
                .nds(form.getNds())
                .phone(form.getPhone())
                .ocpo(form.getOcpo())
                .oked(form.getOked())
                .phoneDirector(form.getPhoneDirector())
                .build();
        companyRepository.save(company);
    }
}
