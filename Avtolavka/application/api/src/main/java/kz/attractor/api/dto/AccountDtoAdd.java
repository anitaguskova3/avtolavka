package kz.attractor.api.dto;

import kz.attractor.datamodel.model.AccountPaymentStatus;
import kz.attractor.datamodel.model.AccountSupplyStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDtoAdd {
    private long orderId;
    private String borrow;
    private AccountPaymentStatus accountPaymentStatus;
    private AccountSupplyStatus accountSupplyStatus;
    private String file;
//    private boolean isFile;
}
