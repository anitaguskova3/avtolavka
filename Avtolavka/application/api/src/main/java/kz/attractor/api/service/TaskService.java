package kz.attractor.api.service;

import kz.attractor.api.dto.TaskAddSubtaskDto;
import kz.attractor.api.dto.TaskDto;
import kz.attractor.api.dto.TaskDtoAdd;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.Task;
import kz.attractor.datamodel.model.TaskPriority;
import kz.attractor.datamodel.model.TaskStatus;
import kz.attractor.datamodel.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository repository;

    public Page<TaskDto> findAll(Pageable pageable) {
        Page<Task> tasks = repository.findAll(pageable);
        return new PageImpl<TaskDto>(
                tasks.getContent().stream()
                        .map(TaskDto::from)
                        .sorted(Comparator.comparing(TaskDto::getPriority).reversed())
                        .collect(Collectors.toList()),
                pageable, tasks.getTotalElements()
        );
    }

    public TaskDto findById(long id) {
        var task = repository.findById(id).orElse(null);
        return TaskDto.from(task);
    }

    public TaskDto update(TaskDto dto){
        var taskOptional = repository.findById(dto.getId());
        if (taskOptional.isEmpty()){
            return null;
        }
        Task task = taskOptional.get();
        task.setName(dto.getName());
        task.setCreateDate(dto.getCreateDate());
        task.setDescription(dto.getDescription());
        task.setDeadline(dto.getDeadline());
        task.setPriority(TaskPriority.valueOfLabel(dto.getPriority()));
        task.setStatus(TaskStatus.valueOfLabel(dto.getStatus()));
        task.setFinish(dto.getFinish());

        return TaskDto.from(repository.save(task));
    }

    public void add(TaskDtoAdd dto){
        Task task = Task.builder()
                .name(dto.getName())
                .description(dto.getDescription())
                .createDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .deadline(dto.getDeadline())
                .priority(TaskPriority.TASK_CURRENT)
                .status(TaskStatus.TASK_NEW)
                .build();
        repository.save(task);
    }

    public void closeStatus(long id) {
        Task task = repository.findById(id).get();
        task.setStatus(TaskStatus.TASK_CLOSED);
        task.setFinish(LocalDate.now());
        repository.save(task);
    }

    public void addSubtask(TaskAddSubtaskDto dto){
        Task parentTask = repository.findById(dto.getParentTaskId()).orElse(null);
        Task task = Task.builder()
                .name(dto.getName())
                .description(dto.getDescription())
                .createDate(LocalDate.now().toString())
                .deadline(dto.getDeadline())
                .priority(TaskPriority.TASK_CURRENT)
                .status(TaskStatus.TASK_NEW)
                .parentTask(parentTask)
                .build();
        repository.save(task);
    }

    public TaskDto findParentTaskOrNull(Long id) {
        Task task = repository.findById(id).orElseThrow(
                () -> new EntityMissingException(Task.class, id));
        var parentTask = task.getParentTask();
        if (parentTask == null) return null;
        return TaskDto.from(parentTask);
    }
}
