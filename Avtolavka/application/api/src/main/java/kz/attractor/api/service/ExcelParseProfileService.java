package kz.attractor.api.service;

import kz.attractor.api.dto.ExcelParseProfileAddDto;
import kz.attractor.datamodel.model.ExcelParseProfile;
import kz.attractor.datamodel.repository.ExcelParseProfileRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class ExcelParseProfileService {
    private final ExcelParseProfileRepository excelParseProfileRepository;

    public void add(ExcelParseProfileAddDto form) {
        if (form.getProfileName().length() == 0) {
            return;
        }
        ExcelParseProfile profile = ExcelParseProfile.builder()
                .profileName(form.getProfileName())
                .rowStart(form.getRowStart())
                .colName(form.getColName())
                .colManufacturer(form.getColManufacturer())
                .colArticle(form.getColArticle())
                .colPrice(form.getColPrice())
                .colQuantity(form.getColQuantity())
                .colUnit(form.getColUnit())
                .defaultQuantity(form.getDefaultQuantity())
                .warehouse(form.getWarehouse())
                .build();
        excelParseProfileRepository.save(profile);
        log.info("В БД добавлен новый профиль: {}", profile.getProfileName());
    }
}
