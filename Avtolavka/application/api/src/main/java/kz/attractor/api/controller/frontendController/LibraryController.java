package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.*;
import kz.attractor.api.service.LibraryService;
import kz.attractor.api.service.PropertiesService;
import kz.attractor.datamodel.repository.LibraryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class LibraryController {
    private final LibraryService libraryService;
    private final PropertiesService propertiesService;
    private final LibraryRepository libraryRepository;

//    @GetMapping("/libraries")
//    public String showLibrariesPage(Model model,
//                                   @RequestParam(required = false) String query,
//                                   @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, size = 20) Pageable pageable,
//                                   HttpServletRequest uriBuilder) {
//        Page<LibraryDto> libraries = libraryRepository.findAll(query, pageable);
//        var uri  = uriBuilder.getRequestURI();
//        constructPageable(libraries, propertiesService.getDefaultPageSize(), model, uri);
//        model.addAttribute("page", libraryService.findAll(query, pageable));
//        if(query != null) {
//            model.addAttribute("link", query);
//        }
//        return "libraries";
//    }
    @GetMapping("/libraries")
    public String showLibraryPage(Model model,
                                @PageableDefault(sort = {"date"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable,
                                  HttpServletRequest uriBuilder) {
        Page<LibraryDto> libraries = libraryService.findAll(pageable);
        var uri  = uriBuilder.getRequestURI();
        constructPageable(libraries, propertiesService.getDefaultPageSize(), model, uri);
        model.addAttribute("page", libraries);
        return "libraries";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink",
                    constructPageUri(uri,
                            list.nextPageable().getPageNumber(),
                            list.nextPageable().getPageSize())
            );
        }
        if (list.hasPrevious()) {
            model.addAttribute("nextPreviousLink",
                    constructPageUri(uri,
                            list.previousPageable().getPageNumber(),
                            list.previousPageable().getPageSize())
            );
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrevious", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        if (uri.contains("?")) {
            return String.format("%s&page=%s&size=%s", uri, page, size);
        }
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping("/libraries/{id}")
    public String showLibraries(@PathVariable long id, Model model) {
        LibraryDto library = libraryService.findById(id);
        model.addAttribute("library", library);
        return "library";
    }

    @GetMapping("/libraries/{id}/edit")
    public String edit(@PathVariable long id, Model model) {
        LibraryDto library = libraryService.findById(id);
        model.addAttribute("form", library);
        return "library-edit";
    }

    @PostMapping("library-edit")
    public String edit(@Valid LibraryDto form,
                       BindingResult validationResult,
                       RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/" + form.getId() + "/edit";
        }
        libraryService.update(form);
        return "redirect:/libraries";
    }

    @GetMapping("/library/add")
    public String add() {
        return "library-add";
    }

    @PostMapping("library-add")
    public String add(@Valid LibraryAddDto form,
                      BindingResult validationResult,
                      RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/library-add";
        }
        libraryService.add(form);
        return "redirect:/libraries";
    }

    @GetMapping("/admin")
    public String admin(){return "admin";}

    @GetMapping("/libraries/{id}/library-delete")
    public String delete(@PathVariable long id, Model model){
        LibraryDto library = libraryService.findById(id);
        model.addAttribute("library", library);
        return "library-delete";
    }

    @PostMapping("library-delete")
    public String delete(@Valid LibraryDto form,
                         BindingResult validationResult,
                         RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/" + form.getId() + "/library-delete";
        }
        libraryService.delete(form);
        return "redirect:/libraries";
    }
}
