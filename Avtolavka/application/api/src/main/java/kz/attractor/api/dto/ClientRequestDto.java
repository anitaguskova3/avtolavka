package kz.attractor.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kz.attractor.datamodel.model.ClientRequest;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientRequestDto {
    private String request;

    public static ClientRequestDto from(ClientRequest request){
        return ClientRequestDto.builder()
                .request(request.getRequest())
                .build();
    }


}
