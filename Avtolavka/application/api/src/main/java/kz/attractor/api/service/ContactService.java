package kz.attractor.api.service;

import kz.attractor.api.dto.ContactDto;
import kz.attractor.api.dto.ContactDtoAdd;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.Contact;
import kz.attractor.datamodel.model.ContactSpecification;
import kz.attractor.datamodel.model.ContactStatus;
import kz.attractor.datamodel.repository.ClientRepository;
import kz.attractor.datamodel.repository.ContactRepository;
import kz.attractor.datamodel.util.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;
    private final ClientRepository clientRepository;

    public List<ContactDto> findAllByClientId(long id) {
        var contacts = contactRepository.findAllByClient_Id(id)
                .stream()
                .map(ContactDto::from)
                .collect(Collectors.toList());
        contacts.sort(Comparator.comparing(ContactDto::getStatus).reversed());
        return contacts;
    }

    public ContactDto findById(long id) {
        var contact = contactRepository.findById(id).orElseThrow( () ->
                new EntityMissingException(Contact.class, id));
        return ContactDto.from(contact);
    }

    public ContactDto update(ContactDto form) {
        var contactOpt = contactRepository.findById(form.getId());
        if (contactOpt.isEmpty()) {
            return null;
        }
        Contact contact = contactOpt.get();
        contact.setName(form.getName());
        contact.setPhoneMain(form.getPhoneMain());
        contact.setEmail1(form.getEmail1());
        contact.setEmail2(form.getEmail2());
        contact.setEmail3(form.getEmail3());
        contact.setEmailMain(form.getEmailMain());
        contact.setPhone1(form.getPhone1());
        contact.setPhone2(form.getPhone2());
        contact.setPhone3(form.getPhone3());
        contact.setPosition(form.getPosition());
        contact.setStatus(ContactStatus.valueOfLabel(form.getStatus()));
        contact.setClient(clientRepository.findById(form.getClientId()).get());
        contact.setDateOfBirth(form.getDateOfBirth());
        return ContactDto.from(contactRepository.save(contact));
    }

    public ContactDto add(ContactDtoAdd form) {
        Contact contact = Contact.builder()
                .name(form.getName())
                .phoneMain(form.getPhone1())
                .phone1(form.getPhone1())
                .phone2(form.getPhone2())
                .phone3(form.getPhone3())
                .emailMain(form.getEmail1())
                .email1(form.getEmail1())
                .email2(form.getEmail2())
                .email3(form.getEmail3())
                .position(form.getPosition())
                .status(ContactStatus.CONTACT_NEW)
                .client(clientRepository.findById(form.getClientId()).get())
                .date(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .dateOfBirth(form.getDateOfBirth())
                .build();
        return ContactDto.from(contactRepository.save(contact));
    }

    public List<ContactDto> findAllSearched(String query) {
        ContactSpecification nameLike = new ContactSpecification(new SearchCriteria("name", ":", query));
        var contacts = contactRepository.findAll(nameLike)
                .stream()
                .map(ContactDto::from)
                .collect(Collectors.toList());
        contacts.sort(Comparator.comparing(ContactDto::getStatus).reversed());
        return contacts;
    }
}
