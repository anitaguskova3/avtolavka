package kz.attractor.api.dto;

import kz.attractor.datamodel.model.FileStory;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileStoryDto {

    private long id;
    private String name;
    private String createDate;

    public static FileStoryDto from(FileStory fileStory){
        return FileStoryDto.builder()
                .id(fileStory.getId())
                .name(fileStory.getName())
                .createDate(fileStory.getCreateDate())
                .build();
    }
}
