package kz.attractor.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ClientAddressAddDto {

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_country;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_city;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_street;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_house;

    private String physical_country;

    private String physical_city;

    private String physical_street;

    private String physical_house;

}