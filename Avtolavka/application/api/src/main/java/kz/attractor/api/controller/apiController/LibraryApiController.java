package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.LibraryDto;
import kz.attractor.api.dto.ProductDto;
import kz.attractor.api.service.LibraryService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/libraries")
public class LibraryApiController {

    private final LibraryService service;

    @GetMapping
    public Page<LibraryDto> findLibraries(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20)
                                                Pageable pageable, String query) {
        return service.findAll(query, pageable);
    }
}
