package kz.attractor.api.service;


import kz.attractor.api.config.StorageProperties;
import kz.attractor.common.exception.StorageException;
import kz.attractor.common.exception.StorageFileNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Component("local")
@AllArgsConstructor
public class FileSystemStorageService implements StorageService {

    private final StorageProperties properties;

    @Override
    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(Path.of(properties.getLocation()));
        } catch (IOException e) {
            throw new StorageException("Невозможно создать хранилище", e);
        }
    }

    @Override
    public void store(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Невозможно загрузить пустой файл");
            }

            Path destinationFile = properties.getPathLocation().resolve(Paths.get(file.getOriginalFilename())).normalize().toAbsolutePath();

            if (!destinationFile.getParent().equals(properties.getPathLocation().toAbsolutePath()))
                throw new StorageException("Нельзя хранить файл за пределами текущего каталога");

            try (InputStream stream = file.getInputStream()) {
                Files.copy(stream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new StorageException("Ошибка в сохранении файла", e);
        }
    }

    @Override
    public Path load(String filename) {
        return properties.getPathLocation().resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable())
                return resource;
            else
                throw new StorageFileNotFoundException("Невозможно считать файл" + filename);
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Невозможно считать файл" + filename);
        }
    }
}
