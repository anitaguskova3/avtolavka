package kz.attractor.api.dto;

import kz.attractor.datamodel.model.ExcelParseProfile;
import kz.attractor.datamodel.model.Warehouse;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExcelParseProfileDto {
    private Long id;
    private String profileName;
    private Integer rowStart;
    private Integer colName;
    private Integer colManufacturer;
    private Integer colArticle;
    private Integer colPrice;
    private Integer colQuantity;
    private Integer colUnit;
    private Integer defaultQuantity;
    private Warehouse warehouse;

    public static ExcelParseProfileDto from(ExcelParseProfile profile) {
        return ExcelParseProfileDto.builder()
                .id(profile.getId())
                .profileName(profile.getProfileName())
                .rowStart(profile.getRowStart())
                .colName(profile.getColName())
                .colManufacturer(profile.getColManufacturer())
                .colArticle(profile.getColArticle())
                .colPrice(profile.getColPrice())
                .colQuantity(profile.getColQuantity())
                .colUnit(profile.getColUnit())
                .defaultQuantity(profile.getDefaultQuantity())
                .warehouse(profile.getWarehouse())
                .build();
    }
}
