package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Library;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LibraryDto {

    private Long id;
    private String title;
    private String subtitle;
    private String date;
    private String descriptionPromo;
    private String imagePromo;
    private String image;
    private String description;
    private String file;

    public static LibraryDto from(Library library){
        return LibraryDto.builder()
                .id(library.getId())
                .title(library.getTitle())
                .subtitle(library.getSubtitle())
                .date(library.getDate())
                .descriptionPromo(library.getDescriptionPromo())
                .imagePromo(library.getImagePromo())
                .image(library.getImage())
                .description(library.getDescription())
                .file(library.getFile())
                .build();
    }
}
