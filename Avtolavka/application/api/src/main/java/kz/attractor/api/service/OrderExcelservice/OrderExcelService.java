package kz.attractor.api.service.OrderExcelservice;

import kz.attractor.api.service.OrderService;
import kz.attractor.datamodel.model.OrderProduct;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.OutputStream;
import java.util.List;

@Service
@AllArgsConstructor
public class OrderExcelService {

    private final OrderService orderService;

    public void exportExcel(String[] headList, long id,
                                   OutputStream outputStream) throws Exception {
        // Создаем книгу Excel
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        // Создаем заголовок
        XSSFRow row = sheet.createRow(0);
        for (int i = 0; i < headList.length; i++) {
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(headList[i]);
        }
        List<OrderProduct> dataList = orderService.findOrderProductsByOrderId(id);
        // добавляем данные
        for (int line = 0; line < dataList.size(); line++) {
            XSSFRow rowData = sheet.createRow(line+1);
            var product = dataList.get(line);
            for (int j = 0; j < dataList.size(); j++) {
                rowData.createCell(0).setCellValue(product.getProduct().getName());
                rowData.createCell(1).setCellValue(product.getUnit());
                rowData.createCell(2).setCellValue(product.getQuantity());
                rowData.createCell(3).setCellValue(product.getPrice().toString());
                rowData.createCell(4).setCellValue(product.getSum());
//                rowData.createCell(3).setCellValue(product.getUnit());

            }
        }
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
        }
}
