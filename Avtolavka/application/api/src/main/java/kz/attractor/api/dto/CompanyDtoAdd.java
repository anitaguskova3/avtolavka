package kz.attractor.api.dto;

import kz.attractor.datamodel.model.ClientBank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDtoAdd {
    @NotBlank(message = "Поле не должно быть пустым")
    private String bin;
    @NotBlank(message = "Поле не должно быть пустым")
    private String name;
    @NotBlank(message = "Поле не должно быть пустым")
    private int kbe;
    @NotBlank(message = "Поле не должно быть пустым")
    private int cato;
    @NotBlank(message = "Поле не должно быть пустым")
    private int ocpo;
    @NotBlank(message = "Поле не должно быть пустым")
    private String nds;
    @NotBlank(message = "Поле не должно быть пустым")
    private String bank;
    @NotBlank(message = "Поле не должно быть пустым")
    private String bik;
    @NotBlank(message = "Поле не должно быть пустым")
    private String iik;
    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_address;
    @NotBlank(message = "Поле не должно быть пустым")
    private String physical_address;
    @NotBlank(message = "Поле не должно быть пустым")
    private String index;
    @NotBlank(message = "Поле не должно быть пустым")
    private String emailBuh;
    @NotBlank(message = "Поле не должно быть пустым")
    private String phone;
    @NotBlank(message = "Поле не должно быть пустым")
    private String krp;
    @NotBlank(message = "Поле не должно быть пустым")
    private String oked;
    @NotBlank(message = "Поле не должно быть пустым")
    private String emailPrice;
    @NotBlank(message = "Поле не должно быть пустым")
    private String fioDirector;
    @NotBlank(message = "Поле не должно быть пустым")
    private String phoneDirector;

}
