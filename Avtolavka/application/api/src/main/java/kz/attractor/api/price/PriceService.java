package kz.attractor.api.price;

import kz.attractor.api.dto.ProductDto;
import kz.attractor.api.price.creator.PriceFileFactory;
import kz.attractor.api.price.sender.PriceSenderFactory;
import kz.attractor.common.enums.PriceFileType;
import kz.attractor.common.enums.SenderType;
import kz.attractor.datamodel.model.Client;
import kz.attractor.datamodel.repository.ClientRepository;
import kz.attractor.datamodel.repository.ProductRepository;
import kz.attractor.emailclient.service.EmailClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class PriceService {

    private final ProductRepository productRepository;
    private final ClientRepository clientRepository;
    private final EmailClient emailClient;

    public void sendPrice(PriceFileType priceFileType, SenderType senderType, List<Long> clientIds) {
        List<ProductDto> products = productRepository.findAll().stream().map(ProductDto::from).collect(Collectors.toList());
        for (Long clientId : clientIds) {
            clientRepository.findById(clientId).ifPresent(c -> sendPrice(priceFileType, senderType, products, c));
        }
    }

    public void sendPrice(PriceFileType priceFileType, SenderType senderType, List<ProductDto> products, Client client) {
        var priceCreator = PriceFileFactory.priceFileCreators.get(priceFileType);
        File file = priceCreator.create(products);
        try {
            var priceSender = PriceSenderFactory.priceSenders.get(senderType);
            priceSender.send(file, client, emailClient);
        } catch (Exception e) {
            log.info("Не удалось отправить прайс: {}", e.getMessage());
        } finally {
            if (file.delete()) {
                log.info("Прайс успешно удален");
            }
        }
    }
}
