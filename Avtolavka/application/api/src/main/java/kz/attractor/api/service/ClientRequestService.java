package kz.attractor.api.service;

import kz.attractor.api.dto.ClientRequestDto;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.ClientRequest;
import kz.attractor.datamodel.repository.ClientRequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClientRequestService {
    private final ClientRequestRepository repository;

    public ClientRequestDto save(ClientRequestDto dto) {
        ClientRequest request = ClientRequest.builder()
                .request(dto.getRequest())
                .build();
        return ClientRequestDto.from(repository.save(request));
    }

    public List<ClientRequestDto> findAll() {
        return repository.findAll()
                .stream()
                .map(ClientRequestDto::from)
                .collect(Collectors.toList());
    }
}
