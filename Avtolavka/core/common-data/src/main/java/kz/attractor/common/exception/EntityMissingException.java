package kz.attractor.common.exception;

public class EntityMissingException extends RuntimeException {
    private static final String ERROR_FORMAT = "Failed to find %s with id '%s'";


    public EntityMissingException(Class<?> entityClass, Object id, Throwable cause) {
        this(String.format(ERROR_FORMAT, entityClass, id), cause);
    }

    public EntityMissingException(Class<?> entityClass, Object id) {
        super(String.format(ERROR_FORMAT, entityClass.getSimpleName(), id));
    }

    public EntityMissingException() {
        super();
    }

    public EntityMissingException(Throwable cause) {
        super(cause);
    }


    public EntityMissingException(String message, Throwable cause) {
        super(message, cause);
    }

}
