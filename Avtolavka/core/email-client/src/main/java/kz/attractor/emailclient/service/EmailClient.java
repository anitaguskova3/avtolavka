package kz.attractor.emailclient.service;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import kz.attractor.emailclient.config.EmailConfigProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Map;

@Component
@AllArgsConstructor
@Slf4j
public class EmailClient implements EmailService {
    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private Configuration configuration;

    private EmailConfigProperties properties;

    @Override
    public void sendMessageAsText(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(properties.getSenderEmail());
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            emailSender.send(message);
        } catch (Exception e) {
            log.error("Ошибка отправки сообщения", e);
        }
    }

    @Override
    public void sendMessageAsHtml(String to,
                                  String subject,
                                  String templatePath,
                                  Map<String, Object> maps) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            helper.setFrom(properties.getSenderEmail());
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(getText(templatePath, maps), true);
            emailSender.send(message);
        } catch (Exception e) {
            log.error("Ошибка отправки сообщения", e);
        }
    }

    private String getText(String templatePath, Map<String, Object> maps) throws IOException, TemplateException {
        var template = configuration.getTemplate(templatePath);
        var text = FreeMarkerTemplateUtils.processTemplateIntoString(template, maps);
        return text;
    }

    @Override
    public void sendMessageWithAttachment(String to,
                                          String subject,
                                          String text,
                                          String attachmentLocation,
                                          String attachmentDescription) {
        try {
            log.info("Отправка сообщения : {} {} ,", to, subject);
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(properties.getSenderEmail());
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);
            FileDataSource file = new FileDataSource(attachmentLocation);
            helper.addAttachment(attachmentDescription, file);
            emailSender.send(message);
        } catch (MessagingException e) {
            log.error("Не удалось отправить сообщение {}", e.getMessage());
        }

    }
}