package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.Handbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface HandbookRepository extends JpaRepository<Handbook, Long>, JpaSpecificationExecutor<Handbook> {
    Page<Handbook> findAll(Pageable pageable);
    Page<Handbook> findAll(Specification<Handbook> specification, Pageable pageable);
    List<Handbook> findAll(Specification<Handbook> specification);
}
