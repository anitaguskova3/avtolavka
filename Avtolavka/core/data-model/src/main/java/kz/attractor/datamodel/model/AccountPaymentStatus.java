package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "payment_status")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountPaymentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "sum_payment")
    private double sumPayment;


}
