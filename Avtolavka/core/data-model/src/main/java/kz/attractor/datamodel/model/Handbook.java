package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "handbook")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Handbook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "article")
    private String article;

    @Column(name = "name")
    private String name;

    @Column(name = "name2")
    private String name2;

    @Column(name = "name3")
    private String name3;

    @Column(name = "name4")
    private String name4;
}
