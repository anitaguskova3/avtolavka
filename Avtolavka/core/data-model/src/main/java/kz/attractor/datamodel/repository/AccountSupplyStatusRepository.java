package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.AccountSupplyStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountSupplyStatusRepository extends JpaRepository<AccountSupplyStatus, Long> {
}
