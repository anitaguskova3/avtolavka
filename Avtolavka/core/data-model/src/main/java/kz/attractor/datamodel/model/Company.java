package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "company")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "bin")
    private String bin;
    @Column(name = "name")
    private String name;
    @Column(name = "kbe")
    private int kbe;
    @Column(name = "cato")
    private int cato;
    @Column(name = "ocpo")
    private int ocpo;
    @Column(name = "nds")
    private String nds;
    @Column(name = "bank")
    private String bank;
    @Column(name = "bik")
    private String bik;
    @Column(name = "iik")
    private String iik;
    @Column(name = "legal_address", columnDefinition = "text")
    private String legal_address;
    @Column(name = "physical_address", columnDefinition = "text")
    private String physical_address;
    @Column(name = "index")
    private String index;
    @Column(name = "emailBuh")
    private String emailBuh;
    @Column(name = "phone")
    private String phone;
    @Column(name = "krp")
    private String krp;
    @Column(name = "oked", columnDefinition = "text")
    private String oked;
    @Column(name = "emailPrice")
    private String emailPrice;
    @Column(name = "fioDirector")
    private String fioDirector;
    @Column(name = "phoneDirector")
    private String phoneDirector;
}
