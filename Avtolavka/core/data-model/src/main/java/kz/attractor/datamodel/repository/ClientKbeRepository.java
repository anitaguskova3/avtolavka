package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.ClientKbe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientKbeRepository extends JpaRepository<ClientKbe, Long> {
    List<ClientKbe> findAll();

}
