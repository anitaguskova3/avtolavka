package kz.attractor.datamodel.model;

import java.util.HashMap;
import java.util.Map;

public enum Unit {

    UNIT_SHT("шт."),
    UNIT_METR("метр"),
    UNIT_UP("уп."),
    UNIT_PAR("пар."),
    UNIT_KG("кг."),
    UNIT_COMP("ком.");

    public final String label;
    private static final Map<String, Unit> BY_LABEL = new HashMap<>();

    static {
        for(Unit unit: values()) {
            BY_LABEL.put(unit.label, unit);
        }
    }
    private Unit(String label) {
        this.label = label;
    }

    public static Unit valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
