package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.Library;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibraryRepository extends JpaRepository<Library, Long> , JpaSpecificationExecutor<Library> {
    Page<Library> findAll(Pageable pageable);
    List<Library> findAll(Specification<Library> specification);
    Page<Library> findAll(Specification<Library> specification, Pageable pageable);
}
