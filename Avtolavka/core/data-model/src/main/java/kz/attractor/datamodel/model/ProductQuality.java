package kz.attractor.datamodel.model;

import java.util.HashMap;
import java.util.Map;

public enum ProductQuality {

    NEW("Новый"),
    VERIFIED("Качественный"),
    DEFECTIVE("Бракованный");

    public final String label;
    private static final Map<String, ProductQuality> BY_LABEL = new HashMap<>();

    static {
        for(ProductQuality quality: values()) {
            BY_LABEL.put(quality.label, quality);
        }
    }
    private ProductQuality(String label) {
        this.label = label;
    }

    public static ProductQuality valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
