package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.FileStory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileStoryRepository extends JpaRepository<FileStory, Long> {
    @Override
    Page<FileStory> findAll(Pageable pageable);
}
