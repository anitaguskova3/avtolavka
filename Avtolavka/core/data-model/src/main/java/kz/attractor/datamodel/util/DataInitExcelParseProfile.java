package kz.attractor.datamodel.util;

import kz.attractor.datamodel.model.ExcelParseProfile;
import kz.attractor.datamodel.repository.ExcelParseProfileRepository;
import kz.attractor.datamodel.repository.WarehouseRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

@Configuration
@AllArgsConstructor
public class DataInitExcelParseProfile {
    private final ExcelParseProfileRepository excelParseProfileRepository;
    private final WarehouseRepository warehouseRepository;
    @Bean
    public CommandLineRunner initExcelParseProfiles() {
        return (args -> Stream.of(excelParseProfiles())
                .peek(System.out::println)
                .forEach(excelParseProfileRepository::save));
    }

    private ExcelParseProfile[] excelParseProfiles() {
        return new ExcelParseProfile[] {
                new ExcelParseProfile(1L, "Акксес прайс лист",
                        7,
                        3,
                        5,
                        8,
                        9,
                        2,
                        6,
                        0,
                         warehouseRepository.getById(1L)),
                new ExcelParseProfile(2L, "ГАЗ прайс лист",
                        10,
                        3,
                        4,
                        8,
                        9,
                        2,
                        6,
                        10,
                        warehouseRepository.getById(2L)),
                new ExcelParseProfile(3L, "AutoOpt.ru прайс лист",
                        11,
                        5,
                        3,
                        7,
                        11,
                        -1,
                        1,
                        0,
                        warehouseRepository.getById(3L)),
                new ExcelParseProfile(4L, "ВАЗ прайс лист",
                        10,
                        3,
                        4,
                        8,
                        9,
                        2,
                        5,
                        5,
                        warehouseRepository.getById(1L))
        };
    }
}
