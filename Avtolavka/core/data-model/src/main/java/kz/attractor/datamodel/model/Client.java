package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "clients")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "resident_client")
    private String resident;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "account_number")
    private String accountNumber;

    @ManyToOne
    @JoinColumn(name = "clientKbe_id")
    private ClientKbe clientKbe;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private ClientAddress address;

    @Column(name = "file")
    private String file;

    @Column(name = "bin")
    private String bin;

    @Column(name = "phone_main")
    private String phoneMain;

    @Column(name = "phone_1")
    private String phone1;

    @Column(name = "phone_2")
    private String phone2;

    @Column(name = "phone_3")
    private String phone3;

    @Column(name = "email_main")
    private String emailMain;

    @Column(name = "email_1")
    private String email1;

    @Column(name = "email_2")
    private String email2;

    @Column(name = "email_3")
    private String email3;

    @Column(name = "status")
    private ClientStatus status;

    @Column(name = "bank")
    private ClientBank bank;

    @Column(name = "createDate")
    private String createDate;

}