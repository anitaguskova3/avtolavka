package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "clientAddress")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "legal_country")
    private String legal_country;

    @Column(name = "legal_city")
    private String legal_city;

    @Column(name = "legal_street")
    private String legal_street;

    @Column(name = "legal_house")
    private String legal_house;

    @Column(name = "physical_country")
    private String physical_country;

    @Column(name = "physical_city")
    private String physical_city;

    @Column(name = "physical_street")
    private String physical_street;

    @Column(name = "physical_house")
    private String physical_house;

}